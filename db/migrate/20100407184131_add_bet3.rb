class AddBet3 < ActiveRecord::Migration
  def self.up
    add_column :bets, :bank,:integer, :default => 0
  end

  def self.down
    remove_column :bets, :bank
  end
end
