class CreateUsers < ActiveRecord::Migration
  def self.up
    create_table :users do |t|
      t.string  :uid
      t.string  :usermail
      t.boolean :deleted, :default => false
      t.integer :ibet_credits, :default => 0
      t.integer :carma, :default => 0
      t.float   :raiting, :default => 0
      t.integer :has_won, :default => 0
      t.integer :has_lost, :default => 0
      
      t.timestamps
    end
    execute %{ALTER TABLE users CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin}

  end

  def self.down
    drop_table :users
  end
end
