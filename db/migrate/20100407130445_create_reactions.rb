class CreateReactions < ActiveRecord::Migration
  def self.up
    create_table :reactions do |t|
      t.references :user
      t.references :bet
      t.text    :answer
      t.integer :stake
      t.boolean :agree
      t.integer :reaction_type

      t.timestamps
    end
    execute %{ALTER TABLE reactions CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin}
  end

  def self.down
    drop_table :reactions
  end
end
