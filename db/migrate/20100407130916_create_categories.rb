class CreateCategories < ActiveRecord::Migration
  def self.up
    create_table :categories do |t|
      t.string :name

      t.timestamps
    end
    execute %{ALTER TABLE categories CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin}
  end

  def self.down
    drop_table :categories
  end
end
