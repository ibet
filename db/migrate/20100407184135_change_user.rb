
class ChangeUser < ActiveRecord::Migration
  def self.up

    change_column :users, :raiting, :integer
    add_column :users, :money,:integer, :default => 0
  end

  def self.down
    remove_column :users, :money
  end
end
