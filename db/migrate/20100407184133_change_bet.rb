
class ChangeBet < ActiveRecord::Migration
  def self.up

    change_column :bets, :start, :datetime
    change_column :bets, :finish, :datetime
    change_column :bets, :finish_stake, :datetime

  end

  def self.down
    #remove_column :bets, :reaction_state
  end
end
