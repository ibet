class AddReaction < ActiveRecord::Migration
  def self.up
    add_column :reactions, :state,:integer
  end

  def self.down
    remove_column :reactions, :state
  end
end
