class AddBet2 < ActiveRecord::Migration
  def self.up
    add_column :bets, :answer,:text
  end

  def self.down
    remove_column :bets, :answer
  end
end
