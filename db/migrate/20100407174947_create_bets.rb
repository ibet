class CreateBets < ActiveRecord::Migration
  def self.up
    create_table :bets do |t|
      t.text :topic
      t.date :start
      t.date :finish
      t.references :user
      t.integer :bet_type
      t.integer :participants, :default => 0
      t.integer :views, :default => 0
      t.references :category
      t.integer :comments, :default => 0

      t.timestamps
    end
    execute %{ALTER TABLE bets CONVERT TO CHARACTER SET utf8 COLLATE utf8_bin}
  end
 
  def self.down
    drop_table :bets
  end
end
