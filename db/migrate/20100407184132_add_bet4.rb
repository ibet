class AddBet4 < ActiveRecord::Migration
  def self.up
    add_column :bets, :reaction_state,:integer
  end

  def self.down
    remove_column :bets, :reaction_state
  end
end
