class AddUser2 < ActiveRecord::Migration
  def self.up
    add_column :users, :permission,:boolean
  end

  def self.down
    remove_column :users, :permission
  end
end
