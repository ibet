class AddBet < ActiveRecord::Migration
  def self.up
    add_column :bets, :state,:integer
    add_column :bets, :last_name,:string
    add_column :bets, :finish_stake, :date
    add_column :bets, :init_stake, :integer
  end

  def self.down
 
    remove_column :bets, :state
    remove_column :bets, :last_name
    remove_column :bets, :finish_stake
    remove_column :bets, :init_stake
  end
end
