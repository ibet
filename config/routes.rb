ActionController::Routing::Routes.draw do |map|
  map.resources :bets

  # The priority is based upon order of creation: first created -> highest priority.

  # Sample of regular route:
  #   map.connect 'products/:id', :controller => 'catalog', :action => 'view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   map.purchase 'products/:id/purchase', :controller => 'catalog', :action => 'purchase'
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   map.resources :products

  # Sample resource route with options:
  #   map.resources :products, :member => { :short => :get, :toggle => :post }, :collection => { :sold => :get }

  # Sample resource route with sub-resources:
  #   map.resources :products, :has_many => [ :comments, :sales ], :has_one => :seller
  
  # Sample resource route with more complex sub-resources
  #   map.resources :products do |products|
  #     products.resources :comments
  #     products.resources :sales, :collection => { :recent => :get }
  #   end

  # Sample resource route within a namespace:
  #   map.namespace :admin do |admin|
  #     # Directs /admin/products/* to Admin::ProductsController (app/controllers/admin/products_controller.rb)
  #     admin.resources :products
  #   end

  # You can have the root of your site routed with map.root -- just remember to delete public/index.html.
  # map.root :controller => "welcome"

  # See how all your routes lay out with "rake routes"

  # Install the default routes as the lowest priority.
  # Note: These default routes make all actions in every controller accessible via GET requests. You should
  # consider removing the them or commenting them out if you're using named routes and resources.
  map.help '/help', :controller => 'user', :action => 'help'
  map.rules '/rules', :controller => 'user', :action => 'rules'
  map.raiting '/raiting', :controller => 'user', :action => 'raiting'
  map.friends '/friends', :controller => 'user', :action => 'friends'
  map.mybets '/mybets', :controller => 'user', :action => 'mybets'
  
  map.reaction 'reaction/:id', :controller => 'user', :action => 'reaction'
  map.show 'show/:id', :controller => 'user', :action => 'bet'
  map.edit 'edit/:id', :controller => 'user', :action => 'edit'
 
  map.new        '/new', :controller => 'user', :action => 'new'
  map.aribterinvite        '/aribterinvite/:id', :controller => 'user', :action => 'aribterinvite'  
  map.create     '/create', :controller => 'user', :action => 'create'
 # map.create_bet '/create_bet', :controller => 'user', :action => 'create_bet'

  map.auth      '/auth', :controller => 'admin', :action => 'auth'
  map.admin     '/admin', :controller => 'admin', :action => 'index'
  map.deleted   '/deleted', :controller => 'admin', :action => 'deleted'
  map.stat      '/stat', :controller => 'admin', :action => 'stat'
  map.signout   '/signout', :controller => 'admin', :action => 'sign_out'
  map.fbdel     '/fbdel', :controller => 'admin', :action => 'fbdelete'
  map.home     '/home', :controller => "user", :action => "index"
  map.root      :controller => "user", :action => "index"

  map.connect ':controller/:action/:id'
  map.connect ':controller/:action/:id.:format'
end
