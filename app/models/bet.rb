class Bet < ActiveRecord::Base
  validates_acceptance_of :terms_of_service, :accept => '1'
  validates_presence_of :answer, :init_stake, :topic
  belongs_to :user
  belongs_to :category
  has_many :reactions


  Bet_state = {
    0 => 'Ожидает',
    1 => 'Отменено',
    2 => 'В процессе',  
    3 => 'Решается',
    4 => 'Закончен'
  }
 
  Bet_type = {
    0 => 'Моно открытый',
    1 => 'Моно закрытый',
    2 => 'Мульти открытый',  
    3 => 'Мульти закрытый'
  }

end
