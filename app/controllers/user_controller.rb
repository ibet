class UserController < ApplicationController
  
  before_filter :source_statistic 
  ensure_application_is_installed_by_facebook_user
  
  before_filter :personal_info
  before_filter :time_checking
  Action_link = FB_app_link  #"/?f=#{FROM_FEEDS}"
  Action_text = "участвуй или проиграешь"
  
  def time_checking
    
    
    #checking if any bet has rejected by stake time or it has finished just by term
    bets = Bet.find(:all)

    bets.each {|bet|
      #if term for stakes has finished
      if WAITING == bet.state && bet.finish_stake <= (Time.now )

        #give money back
        flash[:notice] = "Спор '#{bet.topic}' был просрочен по времени ставок и отменен"
        bet.state = REJECTED
        bet.reaction_state = WAS_CLOSED 
        bet.user.ibet_credits += bet.init_stake
        bet.save
        #money back for the second user, close bet and all reactions  
        unless bet.reactions.blank? 
          bet.reactions.each {|reaction|
            if AGREE == reaction.state &&  USER == reaction.reaction_type
              reaction.user.ibet_credits += reaction.stake
            end
          reaction.state = WAS_CLOSED
          debug_message "reaction #{reaction.id} was closed"
          reaction.save
          }
        end
      end
      #if it was going and now it has finished
      if GOING == bet.state && bet.finish <= (Time.now )
        bet.reaction_state = REQUESTING
        bet.reactions.each { |reaction|
          if AGREE == reaction.state &&  USER == reaction.reaction_type
            reaction.state = REQUESTING
            reaction.save
          end
        }
        #put bet in requesting mode
        bet.state = REQUESTING
        bet.save
      end
    }
  end

  def personal_info
    #getting user's info to use on the layout and so on
    @the_user = User.find_by_uid session[:facebook_session].user.uid
    
    @userF = session[:facebook_session].user
    @access = false
    #in case if ther is no such user or if user does not have e-mail
    if @the_user==nil || @the_user.usermail.blank?
      #check if user has permission
      @access = @userF.has_permission?("publish_stream") && session[:facebook_session].user.has_permission?("email")
    else
      @access = true
    end
    
    if !@access
      render :action => :permission, :layout => false
    end 

  end

  def source_statistic
    if(params[:f]) &&(params[:f].to_i != 0)
      add_stat(0, VISITED, params[:f] ? params[:f].to_i : 0)
    end         
  end
  
  def index
    
    if @access
      @fbuser = User.find(:first, :conditions => ["uid = ?", @userF.uid])
      
      if @fbuser && @fbuser.deleted
        debug_message("User #{session[:facebook_session].user.uid} was already exist but he was in deleted status. So now he is totally deleted and will be added soon")
        @fbuser.destroy
        @fbuser = nil
      end
      #if user does not exist yet in DB, inserting in DB
      if @fbuser == nil
        begin
          #gathering other params
          myParams = {
                        :uid               => @userF.uid,
                        :first_name        => @userF.first_name,
                        :last_name         => @userF.last_name,
                        :usermail          => @userF.email,
                        :ibet_credits      => Start_stake
                      }
        rescue
          debug_message("Cannot get information about #{session[:facebook_session].user.uid}",false)
          render :facebook_error
        else
          #saving user into DB
          @fbuser = User.new(myParams)
          debug_message("User #{session[:facebook_session].user.uid} was added to DB")
          @fbuser.save
          @the_user = User.find_by_uid session[:facebook_session].user.uid
          add_stat(@userF.uid, PERMISSION)       
        #end of getting user info
        end
      #end if @fbuser == nil
      end 
      if @fbuser.usermail.blank?
        @fbuser.update_attributes({:usermail => @userF.email,:first_name => @userF.first_name,:first_name => @userF.first_name,:ibet_credits => Start_stake })
      end
    else
      render :action => :permission, :layout => false
      return
    #end of  if @userF.has_permission?("publish_stream") && ...
    end
    #getting all bets
    @bets = Bet.find(:all, :conditions => {:bet_type => "0"})
  end
    
  #edit bet with id from route
  def edit
  end
  
  #show bet with id from route 
  def bet
    @bet = Bet.find_by_id(params[:id])
    if @the_user.usermail.blank?  || @bet.blank?
      redirect_to :action => "index" 
      return
    end
    
    @second_reaction = Reaction.find( :first, :conditions => ["bet_id = ? AND reaction_type = ? AND (state = ? OR state = ? OR state = ?) ", @bet.id, USER, REQUESTING, WIN, LOST] )
    @bet_might_be_resloved = false
    #fixing result and maybe resolving bet
    if (params[:participant] != nil) && @bet.state == REQUESTING
      @bet_might_be_resloved = false
      #if user is owner of the bet - changing bet state
      p "if #{@bet.user.uid} == #{params[:participant]}"
      if @bet.user.uid == params[:participant]
        if !(params[:wonbutton].blank?)
          @bet.reaction_state = WIN
        elsif !(params[:lostbutton].blank?)
          @bet.reaction_state = LOST
        else
          debug_message "error"
        end
        @bet.save
        #cheking if second reaction is also done
        
        if (@second_reaction.state == LOST) || (@second_reaction.state == WIN)
          p " > > > >> > second is true" 
          @bet_might_be_resloved = true
        end
      #else - finding reaction and change the state
      else
        if !(params[:wonbutton].blank?)
          @second_reaction.state = WIN
        elsif !(params[:lostbutton].blank?)
          @second_reaction.state = LOST
        else
          debug_message "error"
        end
        @second_reaction.save
        if (@bet.reaction_state == LOST) || (@bet.reaction_state == WIN)
          p " > > > >> > bet is true"
          @bet_might_be_resloved = true
        end
      end     
      #if bet is resolved AND condition is resolved and it fits to each other => resolve
      if @bet_might_be_resloved
        p "zzz "* 100
        debugger
        if @second_reaction.state != @bet.reaction_state
          #resolving bet..
          winner = (@bet.reaction_state == WIN ? @bet.user : @second_reaction.user) 
          looser = (@bet.reaction_state != WIN ? @bet.user : @second_reaction.user) 
          winner.has_won += 1
          winner.raiting +=10
          winner.ibet_credits += @bet.bank 
          debug_message "Winner #{winner.id} got bank"
          winner.save

          looser.has_lost += 1
          looser.raiting -= 10
          looser.save
          looser_to_print = 0  
  
      
          @bet.state = FINISHED
          @bet.save

          @second_reaction.save
          #publishing on the feed about result
          session_new = Facebooker::Session.create
          @fb_winner = Facebooker::User.new(winner.uid, session_new)
          @fb_looser = Facebooker::User.new(looser.uid, session_new)
          winner_message = "победил в споре '#{@bet.topic}'"
          looser_message = "проиграл в споре '#{@bet.topic}'"
          
          #begin
            publishing(@fb_winner,
                      @fb_winner,
                      winner_message,
                      Action_text,
                      Action_link,
                      Action_text,
                      Action_link,
                      Action_text,
                      'image',
                      "http://sloboda-studio.com/ibetiamright/images/logo.jpg",
                      Action_link)

            
publishing(@fb_looser,
                      @fb_looser,
                      looser_message,
                      Action_text,
                      Action_link,
                      Action_text,
                      Action_link,
                      Action_text,
                      'image',
                      "http://sloboda-studio.com/ibetiamright/images/logo.jpg",
                      Action_link)
            debug_message "Results was published to feeds"
            flash[:notice] = "Спор '#{@bet.topic}' был успешно разрешен"
          #rescue
            #render :action => :permission
            #return
          #end
          
        else
          p " >>> I'mhere" * 10
          #rearrange bet
          @second_reaction.state = REQUESTING
          @second_reaction.save
          @bet.reaction_state = REQUESTING
          @bet.save
          p "Решения противоречивы, требуется заново решить"
          flash[:notice] = "Решения противоречивы, требуется заново решить"
        #end of  if second_reaction.state != bet.reaction_state
        end
      else
        flash[:notice] = "Теперь требуется решение для другого участника"
      #end of if @bet_might_be_resloved
      end
    #end of if (params[:participant] != nil  )&& @bet.state == REQUESTING  
    end  
   
    #if @bet is resolved check if there is Arbiter
    if REQUESTING == @bet.state
      arbiter_reaction = Reaction.find(:first, :conditions => ["bet_id = ? AND reaction_type = ? AND state = ?", @bet.id, ARBITER, AGREE] )

      user_reaction = Reaction.find(:first, :conditions => ["bet_id = ? AND reaction_type = ? AND (state = ? OR state = ? OR state = ? )", @bet.id, USER, REQUESTING, WIN, LOST] )
      #if user arbiter here or participant withour aribter
      @if_arbiter_exists = (arbiter_reaction != nil)
      
      if @if_arbiter_exists
        #can edit if user arbiter, or user one of the participants and there is no other user
        @can_edit = (arbiter_reaction.user.uid == @the_user.uid)
      else
        #if user one of the participant
        @can_edit = (user_reaction.user.uid == @the_user.uid) ||(@bet.user.uid == @the_user.uid)
      end
      
    end
    debug_message "Checkoing #{params[:yesbutton]} button"
    #if somebody edited reaction(only one time is possible)
    if (!(params[:yesbutton].blank?) || !(params[:nobutton].blank?)) && @bet.state == WAITING
      reaction_params = {
          :answer                   => params[:answer],
          :stake                    => params[:stake], 
          :state                    =>(params[:yesbutton].blank?) ? DISAGREE: AGREE}
      reaction = Reaction.find_by_id(params[:reaction_id])
      reaction.update_attributes(reaction_params)
      debug_message "Reaction #{reaction.id} was updated"
      #if answer is yes - close other similar reactions, check if bet is ready to start
      if AGREE == reaction.state
        #for USER only
        if reaction.reaction_type == USER
          
          #dec credits
          reaction.user.ibet_credits -= params[:stake].to_i
          reaction.user.save
          #adding to the bet
          reaction.bet.bank += params[:stake].to_i
          reaction.bet.save 
          #refresh data
          @bet = Bet.find_by_id(params[:id])
          @the_user = User.find_by_uid session[:facebook_session].user.uid
          debug_message "User  #{reaction.user.uid} agreed to participate as USER"
        else
          debug_message "User  #{reaction.user.uid} agreed to participate as ARBITER"
        end

        ready_user_request = false 
        pending_arbiter_request = false 
        #close similar reaction
        reaction.bet.reactions.each {|one|
          if one.reaction_type == reaction.reaction_type && one.id != reaction.id
            one.state = WAS_CLOSED
            debug_message "Reaction #{one.id} was closed, cause it also #{reaction.reaction_type == USER ? "USER" : "ARBITER" }"
            one.save
          end 
          if one.state == AGREE && one.reaction_type == USER
            ready_user_request = true
          end 
          
          if one.state == WAITING && one.reaction_type == ARBITER
            pending_arbiter_request = true
            debug_message "pending_arbiter_request was set"
          end 
        }
        #check if bet is ready to start
        if ready_user_request && !pending_arbiter_request
          reaction.bet.state = GOING
          reaction.bet.save
          flash[:notice] = "Спор '#{@bet.topic}' начат"
        else
          flash[:notice] = !ready_user_request ? "Вы приняты арбитром, ожидаем участника" : "Ожидаем соглашения арбитром"
        end
      else
        flash[:notice] = "Вы отказались"
      #end of if AGREE == reaction.state
      end 
    #end of if params[:type] != nil
    end

    #@exclude_id = @bet.reactions.map {|v| v.user.uid}.join ","
  #end of def bet
  end

  def create
    #creating bet
    #preparing info

    if !(params[:bet].blank? )
      #!!! it needs to make sure user has enough credits
      params[:bet][:finish] = DateTime.new(params[:finish]["written_on(1i)"].to_i, params[:finish]["written_on(2i)"].to_i,params[:finish]["written_on(3i)"].to_i,params[:finish]["written_on(4i)"].to_i,params[:finish]["written_on(5i)"].to_i) - 3.hours
      params[:bet][:finish_stake] = DateTime.new(params[:finish_stake]["written_on(1i)"].to_i, params[:finish_stake]["written_on(2i)"].to_i,params[:finish_stake]["written_on(3i)"].to_i,params[:finish_stake]["written_on(4i)"].to_i,params[:finish_stake]["written_on(5i)"].to_i) - 3.hours
      params[:bet][:bank] = params[:bet][:init_stake]

      @the_user.ibet_credits -= params[:stake].to_i
      @the_user.save
      @bet = Bet.new(params[:bet])
      p "@bet.valid? = #{@bet.valid?}"
      p @bet.errors
      @bet.user = @the_user
      @bet.state = WAITING
      @bet.bet_type = MONO_OPEN
      @my_way = nil
      if @bet.save
        debugger
        @session_new = Facebooker::Session.create
        if ((process_invitation :user_ids) == ERROR) ||  ((process_invitation :arbiter_ids) == ERROR)
          @my_way = "permission"#render :action => :permission
        end

        flash[:notice] = "Спор '#{@bet.topic}' создан"
        if @my_way == "permission"
          flash[:notice] = "Спор '#{@bet.topic}' удален, так как у вас нет разрешения на печать по стене"
          @bet.destroy
          render :action => :permission
          
        else
          redirect_to :action => "bet", :id => @bet.id
        end
      else
        render :action => :new
        return
      end
    else
      redirect_to :action => :new
    end
    #redirect_to show_url, :id =>  @bet.id   
  end
  
  #new bet
  def new
    p ">" * 100
    p params
    @bet = Bet.new
    #@bet.save
  end

  #Render permission page
  def permission
    redirect_to :action => "index"    
    p "some"
  end

  def create_bet
  end

  def facebook_error
  end
   
  def time_counter(object)
    the_time = Time.now - @time_counter
    @time_counter = Time.now
    p "#{object}: #{the_time.to_s}"
  end

  def publishing(user_from,
                          user_to,
                          main_message,
                          action_link_text,
                          action_link_href,
                          attachment_name,
                          attachment_href,
                          attachment_caption,
                          media_type,
                          media_src,
                          media_href)
    begin
      user_from.publish_to(user_to,
        :message => main_message,
        :action_links => [
                {
                :text => action_link_text,
                :href => action_link_href}
                ],
        :attachment => {
                :name => attachment_name,
                :href => attachment_href,
                :caption => attachment_caption,

                :media => [
                        {
                          :type => media_type,
                          :src =>  media_src,
                          :href => media_href
                        }

                     ]})
      rescue
        debug_message "error with publish_to happened"
        return ERROR
      end
    return true
  #end of def publishing
  end
  def aribterinvite
    @bet = Bet.find_by_id(params[:id])
    @exclude_id = (@bet.reactions.map {|v| v.user.uid}).join ","
  end

  def resolve_bet
  end
 
  def friends
    friends = session[:facebook_session].user.friends
    @userF = session[:facebook_session].user 
    list_of_friends = @userF.friends.map {|val| val.uid}
   
    @users = User.find_all_by_uid(list_of_friends)
    p @users
    render :action => :raiting

  end 
  
  def mybets 
    #select bets where I'm an owner
    @my_bets = Bet.find_all_by_user_id (@the_user.id)
    #select bets where I'm in reaction
    #1. find my reactions
    my_reactions = Reaction.find_all_by_user_id @the_user.id
    p my_reactions
    unless my_reactions.blank?
      list_of_bets = my_reactions.map {|reaction| reaction.bet.id}
      #2. find bet's with my reactions
      unless list_of_bets.blank?
        @reaction_bets = Bet.find_all_by_id (list_of_bets)
      end
    end
  end

  def raiting
    @users = User.find(:all, :order => "raiting DESC")
  end 

  def process_invitation (type)
    debug_message "process_invitation for #{type}"

    if params[type] != nil
      debug_message "Invitation = #{params[type]} "
      #ids = params[:ids].split ","
      #debug_message "we have #{ids.size}  at all =  #{ids}"
      for id in params[type] 
        debug_message "Doing for  = #{id}"
        #if user do not exist - create for him account
        @fbuser = User.find_by_uid id
        if  nil == @fbuser
          @fbuser = User.new(:uid => id)
          @fbuser.save  
          debug_message "user {@fbuser.uid} was created"
        end 
        
        #reaction creating
        @reaction = Reaction.new
        @reaction.user = @fbuser
        @reaction.bet = @bet
        @reaction.state = WAITING
        message = ""
        if :arbiter_ids == type 
          @reaction.reaction_type  = ARBITER
          message = "Я приглашаю тебя быть арбитром в моем споре '#{@bet.topic}'"
        elsif :user_ids == type 
          @reaction.reaction_type  = USER
          message = "Я приглашаю тебя быть участником в моем споре '#{@bet.topic}'"
        else
          debug_message "Wrong type of user... "
        end
        @reaction.save

        action_text = "Принять участие"
        action_link = "#{FB_app_link}/show/#{@bet.id}"
        reciever =  Facebooker::User.new(@fbuser.uid, @session_new)
        addresser = Facebooker::User.new(@the_user.uid, @session_new)
     
        if  ERROR == publishing(addresser,
                    reciever,
                    message,
                    action_text,
                    action_link,
                    action_text,
                    action_link,
                    action_text,
                    'image',
                    "http://sloboda-studio.com/ibetiamright/images/logo.jpg",
                    action_link)
          debug_message "ERROR 1"
          #render :action => :permission
           return ERROR
          end
        
        debug_message "@reaction for {@fbuser.uid} was created"
        flash[:notice] = "Приглашения для спора '@bet.topic' отправлены"
        return true
      end
    end
  end

  def help
  end
  
  def rules
  end
end
