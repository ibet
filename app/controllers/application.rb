# Filters added to this controller apply to all controllers in the application.
# Likewise, all the methods added will be available for all controllers.

class ApplicationController < ActionController::Base
  helper :all # include all helpers, all the time

  # See ActionController::RequestForgeryProtection for details
  # Uncomment the :secret if you're not using the cookie session store
  #protect_from_forgery  :secret => '0fcc823b7dcd071c84967c35c10e155c'
  before_filter :instantiate_controller_and_action_names
  
  def instantiate_controller_and_action_names
    @current_action = action_name
    @current_controller = controller_name

    
  end

  def debug_message (message,is_not_error = true)
    if true #Debug_message_on
      if !is_not_error
        p "<>" * 25
        p "> > > DEBUG ERROR:"
        p "<>" * 25
      end
      p " >" * 25
      p "          " + message
      p " >" * 25
    end
  end

  def add_stat(uid, action, extra = 0)
     myParams = {
                :uid               => uid,
                :action            => action,
                :extra             => extra,
                }
      @stat = Stat.new(myParams)
      @stat.save  
      

      case action
        when INSTALLED
          debug_message ("STAT UPDATED: #{uid} installed application")
        when PERMISSION 
          debug_message ("STAT UPDATED: #{uid} gave permission")
  
        when DELETED
          debug_message ("STAT UPDATED: #{uid} deleted application")
       
        when PUBLISHED
          debug_message ("STAT UPDATED: #{uid} published #{extra} fields")
              
        when VISITED
          case extra
            when FROM_FB_ADS
              @source = "FROM_FB_ADS"
            when
              @source = "FROM_PLAY_ADS" 
            when
              @source = "FROM_PLAY_SITE"
            when
              @source = "FROM_STATUS"
            when
              @source = "FROM_RESERVED"
              
          end
        debug_message ("STAT UPDATED: #{uid} came from #{@source}")
      end
      
  end 
 
end
