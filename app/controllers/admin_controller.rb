class AdminController < ApplicationController
  require 'csv'
  #administration controller
  
  
  def convert_birthday_to_number(db_birthday)
    ((DateTime.now - db_birthday)/365).to_i
  end
  
  def query_prepary (query_type) 
    #debugger
    @query_string = "deleted = :deleted"
    @query_params = Hash.new(0)  
    
    case (query_type)    
      when Deleted
        @query_params[:deleted] = Deleted
        #@query_string = @query_string + " AND friends_invited >= :friends_invited"
      when UnDeleted
        @query_params[:deleted] = UnDeleted
        #@query_params[:invited_enough] = true
        #@query_string = @query_string + " AND invited_enough = :invited_enough"
      when UnInvited
        @query_params[:deleted] = UnDeleted
        #@query_params[:invited_enough] = false
        #@query_string = @query_string + " AND invited_enough = :invited_enough"
    end
    
    #gender querry
    if params[:gender] && params[:gender] != "0"    
      if params[:gender] == "Undefined"
        @query_params[:gender] = ""
      else
        @query_params[:gender] = params[:gender]
      end   
      @query_string = @query_string + " AND gender = :gender"
    end
    #relation querry
    if params[:relation] && params[:relation] != "0"    
      if params[:relation] == "Undefined"
        @query_params[:relation] = ""
      else
        @query_params[:relation] = params[:relation]  
      end
      @query_string = @query_string + " AND relation = :relation"
    end
    #age querry
    if params[:age] && params[:age] != "0"
      if params[:age] == "Undefined"
        @query_string = @query_string + " AND birthday is NULL"
      else
        if params[:age] != "35"
          birthday_start  = Time.now - (params[:age].to_i + 1) * 1.year   
          birthday_finish = Time.now - (params[:age].to_i ) * (1.year) 
        else
          birthday_start  = Time.now - 100.year
          birthday_finish = Time.now - 35.year
        end 
        @query_params[:birthday_start]  = birthday_start
        @query_params[:birthday_finish] = birthday_finish
        @query_string = @query_string + " AND birthday >= :birthday_start  AND birthday <= :birthday_finish"
      end     
    end
  #end def query_prepary (deleted)
  end 
  #authification
  def auth
    if (params[:login])
      if (params[:login] == FBlogin) and (params[:password] == FBpassword)
        cookies[:auth] = "ok" 
        flash[:notice] = 'Successfully logged in'                  
        respond_to do |format|
          format.html { redirect_to admin_url }
        end        
      else
        flash[:notice] = 'Incorrect login and/or password'
      end
    end    
  #def auth 
  end
  def sign_out
    cookies[:auth] = "false" 
    respond_to do |format|
      format.html { render :action => "auth" }
    end
  end
  #main page of showing  undeleted users
  def index   
          
    @query_type = UnDeleted
    todo = ""   
    if (cookies[:auth] != "ok")
      todo = "auth"   
    else
      select_age_preparation        
      query_prepary(UnDeleted)
      @total_number = User.count(:conditions => [@query_string ,@query_params])
      @fbusers = User.paginate(:per_page => Person_per_page, :page => params[:page], :conditions => [@query_string ,@query_params], :order => "created_at")
       
    #end if (cookies[:auth] != "ok")
    end
    #generating of page
    respond_to do |format|
      if todo == "auth"
       format.html { render :action => "auth" }
      else 
        format.html # index.html.erb
        format.xml  { render :xml => @fbusers }
      end
    end
  #end def index 
  end
  #page for showing deleted users
  def deleted
    @query_type = Deleted
    todo = ""
    if (cookies[:auth] != "ok")
       todo = "auth"   
    else    
      select_age_preparation  
      #creating querry  
      query_prepary(Deleted)
      @total_number = User.count(:conditions => [@query_string ,@query_params])
      @fbusers = User.paginate(:per_page => Person_per_page, :page => params[:page], :conditions => [@query_string ,@query_params], :order => "created_at")     
    #end if (cookies[:auth] != "ok")
    end  
    #generation of view
    respond_to do |format|
      if todo == "auth"
       format.html { render :action => "auth" }
      else 
        format.html # index.html.erb
        format.xml  { render :xml => @fbusers }
      end
    end
  #end deleted
  end

  #deleting from facebook call-back function
  def fbdelete
    p "deleting"
    if params[:fb_sig_user]
      @fbuser = User.find(:first ,:conditions => ["uid = ?", params[:fb_sig_user]])
      unless @fbuser.blank?
        @fbuser.update_attributes(:deleted => 1)
        debug_message("User #{params[:fb_sig_user]}  was marked as DELETED")
        add_stat(params[:fb_sig_user],DELETED)
      end
    end
  end

  #preparing select module for age
  def select_age_preparation 
    @selectParams = []
    @selectParams.push ["all",0]  
    13.upto(34) {|i| @selectParams.push [i.to_s,i.to_s]}
    @selectParams.push ["35+","35"] 
    @selectParams.push ["Undefined","Undefined"]
  end 
 
  
  def stat
    #Date statistics
    current_date = Date.new(2010,4,2)
    today_now = Time.now 
    today = Date.new(today_now.year,today_now.month,today_now.day)
    @date_range = (current_date..today).to_a
    @data_pack = []
    @total    = Hash.new(0)
    @date_range.each {|date|
      @query_params = Hash.new(0)
      @query_params[:created_at] = date
      @query_string = "DATE(created_at) = :created_at" 
      @statistic  = Stat.find(:all, :conditions => [@query_string ,@query_params])
      one_pack = Hash.new(0)
      @statistic.each {|stat|      
        case stat[:action] 
          
          when INVITED_NUMBER, PUBLISHED
            one_pack[stat[:action]]+= stat[:extra]
            @total [stat[:action]]+=stat[:extra]
          when PERMISSION, DELETED, INSTALLED
            one_pack[ stat[:action]]+= 1
            @total [stat[:action]]+=1
          when VISITED
            #case stat[:extra]      
              #when FROM_INVITATION, FROM_FEED
                 one_pack[SHIFT + stat[:extra]]+= 1
                 @total [SHIFT + stat[:extra]]+=1
             #end
        end
    
      }
      @data_pack.push one_pack
   }
   @data_pack.reverse!
   #Hour statistic
   @hour_statistic = []
   @hour_deleted_statistic = []
   
   @i = (0..23).to_a
   @query_string = "HOUR(created_at) = ?" 
   @i.each {|i|
     hour_number = User.count(:conditions => ["HOUR(created_at) = ?" , i])  
     @hour_statistic.push hour_number
     
     hour_deleted_number = Stat.count(:conditions => ["HOUR(created_at) = ? AND action = #{DELETED}" , i])  
     @hour_deleted_statistic.push hour_deleted_number
  }
  end

  def statistic_to_csv
    stat
    @data_pack.reverse!

    @titles  = "Date,Installations,Disabled Users,Published,FB ads,A ads,B ads,FB status,FB feed,I,II,III".split(",")
    report = StringIO.new
    CSV::Writer.generate(report, ',') do |title|
      title << @titles
      
      @data_pack.each_with_index {|data, i|       
        title <<  [@date_range[i],data[PERMISSION],data[DELETED],data[PUBLISHED],data[SHIFT + FROM_FB_ADS],data[SHIFT + FROM_PLAY_ADS],data[SHIFT + FROM_PLAY_SITE],data[SHIFT + FROM_STATUS],data[SHIFT + FROM_FEEDS],data[SHIFT + FROM_NOTIFICATIONS],data[SHIFT + FROM_OTHER1],data[SHIFT + FROM_OTHER2]]

      }
    end   
    report.rewind

    time_now = Time.now.to_formatted_s(:number)
    file = File.open("#{RAILS_ROOT}/public/#{time_now}.csv", "wb")
    file.write(report.read)
    file.close
    
    redirect_to "/#{time_now}.csv"
  #end of statistic_to_csv
  end

  def export_table_to_csv
    query_prepary(params[:query_type].to_i) 

    columns = "first_name,last_name,created_at,birthday,gender,usermail,relation,hometown_location,networking,deleted,uid,current_location,affiliations,friends_published"
    @results = User.find(:all,:select => columns, :conditions => [@query_string ,@query_params],:order => "created_at")
    @titles  = "First name,Last name,Permission date,Birthday,Gender,e-mail,Relation,Hometown location,Networks,If deleted,Facebook id,Current location,Affiliations,Published on walls,Age".split(",")
    report = StringIO.new
    CSV::Writer.generate(report, ',') do |title|
      title << @titles
      @titles  = columns.split(",")
      @results.each do |result|
        
        result[:birthday] = result[:birthday] 
        z = @titles.map { |a| result.send(a) }           
        age = result[:birthday] ? convert_birthday_to_number(result[:birthday]) : nil    
        z = z + [age]
        title << z
      end
    end   
    report.rewind

    time_now = Time.now.to_formatted_s(:number)
    file = File.open("#{RAILS_ROOT}/public/#{time_now}.csv", "wb")
    file.write(report.read)
    file.close
    
    redirect_to "/#{time_now}.csv"
  #end of export_table_to_csv
  end
  
end
